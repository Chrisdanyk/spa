package cd.learning.spa.repository;

import cd.learning.spa.entity.Auteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuteurRepository extends JpaRepository<Auteur, Long> {

    public List<Auteur> findByNom(String nom);

    @Query("select a from Auteur a")
    public List<Auteur> getAllAuteursWithQuery();
}
