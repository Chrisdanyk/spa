package cd.learning.spa.repository;

import cd.learning.spa.entity.Address;
import cd.learning.spa.entity.Auteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
    @Query("select a from Address a")
     List<Address> getAllAddressesWithQuery();
}
