package cd.learning.spa.service;

import cd.learning.spa.entity.Auteur;
import cd.learning.spa.repository.AuteurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class AuteurService {
    @Autowired
    private AuteurRepository auteurRepository;

    public Page<Auteur> findAllAuteurs(PageRequest pageRequest) {
        return auteurRepository.findAll(pageRequest);
    }

    public Optional<Auteur> findAuteurById(Long id) {
        return auteurRepository.findById(id);
    }

    public List<Auteur> findAuteursByNom(String nom) {
        return auteurRepository.findByNom(nom);
    }

    public List<Auteur> findAllAuteursWithQuery() {
        return auteurRepository.getAllAuteursWithQuery();
    }

    public Auteur saveAuteur(Auteur auteur) {
        return auteurRepository.save(auteur);
    }

    public Auteur editAuteur(Auteur auteur) {
        return auteurRepository.save(auteur);
    }

    public void deleteAuteur(Auteur auteur) {
        auteurRepository.delete(auteur);
    }


}
