package cd.learning.spa.service;

import cd.learning.spa.entity.MyUserDetails;
import cd.learning.spa.entity.User;
import cd.learning.spa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Optional<User> foundUser = userRepository.findByUserName(userName);

        foundUser.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));

        MyUserDetails foundUserDetails = new MyUserDetails(foundUser.get());
        System.out.println("username: " + foundUserDetails.getUsername() + " pwd: " + foundUserDetails.getPassword() + " auth: " + foundUserDetails.getAuthorities().toString());
        return foundUser.map(MyUserDetails::new).get();
    }

}
