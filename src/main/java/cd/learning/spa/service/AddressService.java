package cd.learning.spa.service;

import cd.learning.spa.entity.Address;
import cd.learning.spa.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {
    @Autowired
    private AddressRepository addressRepository;

    public List<Address> findAllAddresss() {
        return addressRepository.findAll();
    }

    public Optional<Address> findAddressById(Long id) {
        return addressRepository.findById(id);
    }

//    public List<Address> findAddresssByNom(String nom) {
//        return addressRepository.findByNom(nom);
//    }

    public List<Address> findAllAddresssWithQuery() {
        return addressRepository.getAllAddressesWithQuery();
    }

    public Address saveAddress(Address address) {
        return addressRepository.save(address);
    }

    public Address editAddress(Address address) {
        return addressRepository.save(address);
    }

    public void deleteAddress(Address address) {
        addressRepository.delete(address);
    }


}
