package cd.learning.spa;

import cd.learning.spa.entity.Auteur;
import cd.learning.spa.service.AuteurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.Random;

@SpringBootApplication
public class SpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpaApplication.class, args);
    }

}

//@Component
//class DataLoader implements CommandLineRunner {
//    @Autowired
//    private AuteurService auteurService;
//
//    @Override
//    public void run(String... args) throws Exception {
////        Auteur auteur1 = new Auteur();
////        Auteur auteur2 = new Auteur();
////        auteur1.setNom("chris dany");
////        auteur2.setNom("elie djani");
////        auteur1.setEmail("chrisdany9@gmail.com");
////        auteur2.setEmail("eliedjani@gmail.com");
//        Random random = new Random();
//        for (int i = 0; i < 100; i++) {
//            Auteur a = new Auteur("chris" + random.nextInt(1000), "mail@mail.com" + random.nextInt(1000));
//            auteurService.saveAuteur(a);
//        }
////        auteurService.saveAuteur(auteur1);
////        auteurService.saveAuteur(auteur2);
//
//    }
//}
