package cd.learning.spa.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class HomeController {

    @GetMapping("/")
    public String index(){
        return "Bienvenue les Godzila à notre premier app springboot";
    }

    @GetMapping("/hello")
    public String welcome(){
        return "Vous pouvez y aller";
    }
}
