package cd.learning.spa.controller;

import cd.learning.spa.entity.Auteur;
import cd.learning.spa.exception.ResourceNotFoundException;
import cd.learning.spa.service.AuteurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class AuteurController {
    @Autowired
    private AuteurService auteurService;

    @GetMapping("/auteurs")
    public ResponseEntity<Page<Auteur>> getAllAuteurs(@RequestParam("page") Optional<Integer> page, Optional<String> sortby) {
        PageRequest pageRequest = PageRequest.of(page.orElse(0), 20, Sort.Direction.ASC, sortby.orElse("id"));
        return ResponseEntity.ok(auteurService.findAllAuteurs(pageRequest));
    }

    @GetMapping("/auteurs/{id}")
    public ResponseEntity<Auteur> getAuteur(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(auteurService.findAuteurById(id).orElseThrow(() -> new ResourceNotFoundException("auteur with this id not found!!!")));
    }

    @GetMapping("/auteurss/{nom}")
    public ResponseEntity<List<Auteur>> getAuteur(@PathVariable(name = "nom") String nom) {
        return ResponseEntity.ok(auteurService.findAuteursByNom(nom));
    }

    @GetMapping("/auteurs/all")
    public ResponseEntity<List<Auteur>> getAllAuteursWithQuery() {
        return ResponseEntity.ok(auteurService.findAllAuteursWithQuery());
    }


    @PostMapping("/auteurs")
    public ResponseEntity<Auteur> saveAuteur(@RequestBody Auteur auteur) {
        return ResponseEntity.ok(auteurService.saveAuteur(auteur));
    }

    @PutMapping("/auteurs/{id}")
    public ResponseEntity editAuteur(@RequestBody Auteur auteur, @PathVariable("id") Long id) {
        Auteur foundAuteur = auteurService.findAuteurById(id)
            .orElseThrow(() -> new ResourceNotFoundException("auteur with this id not found!!!"));
        foundAuteur.setEmail(auteur.getEmail());
        foundAuteur.setNom(auteur.getNom());
        Auteur updatedAuteur = auteurService.saveAuteur(foundAuteur);
        return ResponseEntity.ok(updatedAuteur);
    }

    @DeleteMapping("/auteurs/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteAuteur(@PathVariable("id") Long id) {
        Auteur foundAuteur = auteurService.findAuteurById(id).orElseThrow(() -> new ResourceNotFoundException("auteur with this id not found!!!"));
        auteurService.deleteAuteur(foundAuteur);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);

    }
}
