package cd.learning.spa.controller;

import cd.learning.spa.entity.Address;
import cd.learning.spa.exception.ResourceNotFoundException;
import cd.learning.spa.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class AddressController {
    @Autowired
    private AddressService addressService;

    @GetMapping("/addresss")
    public ResponseEntity<List<Address>> getAllAddresss() {
        return ResponseEntity.ok(addressService.findAllAddresss());
    }

    @GetMapping("/addresss/{id}")
    public ResponseEntity<Address> getAddress(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(addressService.findAddressById(id).orElseThrow(() -> new ResourceNotFoundException("address with this id not found!!!")));
    }

//    @GetMapping("/addressss/{nom}")
//    public ResponseEntity<List<Address>> getAddress(@PathVariable(name = "nom") String nom) {
//        return ResponseEntity.ok(addressService.findAddresssByNom(nom));
//    }

    @GetMapping("/addresss/all")
    public ResponseEntity<List<Address>> getAllAddresssWithQuery() {
        return ResponseEntity.ok(addressService.findAllAddresssWithQuery());
    }


    @PostMapping("/addresss")
    public ResponseEntity<Address> saveAddress(@RequestBody Address address) {
        return ResponseEntity.ok(addressService.saveAddress(address));
    }

//    @PutMapping("/addresss/{id}")
//    public ResponseEntity editAddress(@RequestBody Address address, @PathVariable("id") Long id) {
//        Address foundAddress = addressService.findAddressById(id)
//                .orElseThrow(() -> new ResourceNotFoundException("address with this id not found!!!"));
//        foundAddress.setEmail(address.getEmail());
//        foundAddress.setNom(address.getNom());
//        Address updatedAddress = addressService.saveAddress(foundAddress);
//        return ResponseEntity.ok(updatedAddress);
//    }

    @DeleteMapping("/addresss/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteAddress(@PathVariable("id") Long id) {
        Address foundAddress = addressService.findAddressById(id).orElseThrow(() -> new ResourceNotFoundException("address with this id not found!!!"));
        addressService.deleteAddress(foundAddress);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);

    }
}
