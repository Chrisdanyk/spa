package cd.learning.spa.security;

import cd.learning.spa.entity.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class JWTTokenProvider {

    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    private String secret = "youwillneverguess";

    public String generateJwtToken(User userPrincipal) {
//        String[] claims = getClaimsFromUser(userPrincipal);
        return JWT.create()
            .withIssuer("GET_ARRAYS_LLC")
            .withAudience("GET_ARRAYS_ADMINISTRATION")
            .withIssuedAt(new Date())
            .withClaim("roles", userPrincipal.getRoles())
            .withSubject(userPrincipal.getUserName())
            .withExpiresAt(new Date(System.currentTimeMillis() + 60 * 4 * 1000))
            .sign(Algorithm.HMAC512(secret.getBytes()));
    }

    private String[] getClaimsFromUser(UserDetails userPrincipal) {
        List<String> authorities = new ArrayList<>();
        for (GrantedAuthority authority : userPrincipal.getAuthorities()) {
            authorities.add(authority.getAuthority());
        }
        return authorities.toArray(new String[0]);
    }
//
    public List<GrantedAuthority> getAuthorities(String token) {
        String[] claims = getClaimsFromToken(token);
        return Arrays.stream(claims).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }
//
    private String[] getClaimsFromToken(String token) {
        JWTVerifier verifier = getJWTVerifier();
        Claim claim = verifier.verify(token).getClaim("roles");
        String[] roles = claim.toString().split(",");
        return roles;
    }

    public Authentication getAuthentication(String username, List<GrantedAuthority> authorities, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, null, authorities);
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        return authenticationToken;
    }

    public boolean isTokenValid(String username, String token) {
        JWTVerifier jwtVerifier = getJWTVerifier();
        return StringUtils.isNotEmpty(username) && isTokenExpired(jwtVerifier, token);
    }

    private boolean isTokenExpired(JWTVerifier jwtVerifier, String token) {
        Date expiration = jwtVerifier.verify(token).getExpiresAt();
        return !expiration.before((new Date()));
    }

    public String getSubject(String token) {
        JWTVerifier jwtVerifier = getJWTVerifier();
        return jwtVerifier.verify(token).getSubject();
    }

    private JWTVerifier getJWTVerifier() {
        JWTVerifier verifier;
        try {
            Algorithm algorithm = Algorithm.HMAC512(secret);
            verifier = JWT.require(algorithm).withIssuer("GET_ARRAYS_LLC").build();
        } catch (Exception ex) {
            throw new JWTVerificationException("TOKEN_CANNOT_BE_VERIFIED");
        }
        return verifier;
    }
}
